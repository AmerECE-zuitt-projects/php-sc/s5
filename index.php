<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S5: Activity</title>
</head>
<body>
    <?php session_start();?>

    <?php if (!isset($_SESSION["email"])): ?>
        <form action="./server.php" method="POST">
            <input type="hidden" name="action" value="login">
            Email: <input type="email" name="email" requires />
            Password: <input type="password" name="password" required />
            <button type="submit">Login</button>
        </form>

        <?php if (isset($_SESSION["error"])): ?>
            <p><?=$_SESSION["error"];?></p>
            <?php unset($_SESSION["error"]);?>
        <?php endif;?>

    <?php else: ?>
        <p>Hello <?=$_SESSION["email"];?></p>
        <form action="./server.php" method="POST">
            <input type="hidden" name="action" value="logout">
            <button type="submit">logout</button>
        </form>
    <?php endif;?>
</body>
</html>